[Code]
 var
  vcRedist64BitPath: string;
  bVcRedist64BitNeeded : boolean;

 const 
  vcRedist64BitURL = 'https://aka.ms/vs/16/release/vc_redist.x64.exe';
  function OnDownloadProgress(const Url, Filename: string; const Progress, ProgressMax: Int64): Boolean;
begin
  if ProgressMax <> 0 then
    Log(Format('  %d of %d bytes done.', [Progress, ProgressMax]))
  else
    Log(Format('  %d bytes done.', [Progress]));
  Result := True;
end;
 function IsVCRedist64BitNeeded(): boolean;
var
  strVersion: string;

begin
  if (RegQueryStringValue(HKEY_LOCAL_MACHINE,
    'SOFTWARE\Microsoft\VisualStudio\14.0\VC\Runtimes\x64', 'Version', strVersion)) then
  begin
    // Is the installed version at least 14.14 ? 
    Log('VC Redist x64 Version : found ' + strVersion);
    Result := (CompareStr(strVersion, 'v14.27.29016.00') < 0);
  end
  else
  begin
    // Not even an old version installed
    Log('VC Redist x64 is not already installed');
   
  end;
end;
function PrepareToInstall(var NeedsRestart: Boolean): String;
var
  ResultCode: integer;
begin
  result := '';

    bVcRedist64BitNeeded := IsVCRedist64BitNeeded();
    if(bVcRedist64BitNeeded) then
  begin
    // We need to download the 64 Bit VC Redistributable from the Microsoft Website
    vcRedist64BitPath := ExpandConstant('{tmp}\vc_redist_x64.exe');
    DownloadTemporaryFile(vcRedist64BitURL, 'vc_redist_x64.exe', '',@OnDownloadProgress);
    if Exec(ExpandConstant(vcRedist64BitPath), '/install /passive /norestart', '',
          SW_SHOW, ewWaitUntilTerminated, ResultCode) then begin
        // handle success if necessary; ResultCode contains the exit code
        if not (ResultCode = 0) then begin
          MsgBox(ExpandConstant('The installation of Visual Studio x64 Redistributable failed. The Meeting Schedule Assistant installation will be aborted.'), mbInformation, MB_OK);
          Abort();
        end;
      end
  end;
end;



