#include "Common.h"
#include "StlThumbnail.h"
#include "gdiplus.h"
#include "fstream"

using namespace Gdiplus;
CThumbnailProvider::CThumbnailProvider()
{
    DllAddRef();
    m_cRef = 1;
    m_pSite = NULL;
}


CThumbnailProvider::~CThumbnailProvider()
{
    if (m_pSite)
    {
        m_pSite->Release();
        m_pSite = NULL;
    }
    DllRelease();
}


STDMETHODIMP CThumbnailProvider::QueryInterface(REFIID riid,
                                                void** ppvObject)
{
    static const QITAB qit[] = 
    {
        QITABENT(CThumbnailProvider, IInitializeWithStream),
        QITABENT(CThumbnailProvider, IThumbnailProvider),
        QITABENT(CThumbnailProvider, IObjectWithSite),
        {0},
    };
    return QISearch(this, qit, riid, ppvObject);
}


STDMETHODIMP_(ULONG) CThumbnailProvider::AddRef()
{
    LONG cRef = InterlockedIncrement(&m_cRef);
    return (ULONG)cRef;
}


STDMETHODIMP_(ULONG) CThumbnailProvider::Release()
{
    LONG cRef = InterlockedDecrement(&m_cRef);
    if (0 == cRef)
        delete this;
    return (ULONG)cRef;
}


STDMETHODIMP CThumbnailProvider::Initialize(IStream *pstm, 
                                            DWORD grfMode)
{
    
    STATSTG stat;
    if (pstm->Stat(&stat, STATFLAG_DEFAULT) != S_OK) {
        return S_FALSE;
    }
    PixelsData = new char[stat.cbSize.QuadPart];
    if (pstm->Read(PixelsData, stat.cbSize.QuadPart, &len) != S_OK) {
        return S_FALSE;
    }
    std::fstream binout;
    return S_OK;
}


STDMETHODIMP CThumbnailProvider::GetThumbnail(UINT cx, 
                                              HBITMAP *phbmp, 
                                              WTS_ALPHATYPE *pdwAlpha)
{
	*phbmp = NULL; 
	*pdwAlpha = WTSAT_UNKNOWN;
	ULONG_PTR token;
	GdiplusStartupInput input;
	if (Ok == GdiplusStartup(&token, &input, NULL))
	{
		//gcImage.LogBuffer();
        int BitmapWidth = sqrt(len / 2);
		Bitmap * pBitmap = new Bitmap(BitmapWidth, BitmapWidth);
        if( pBitmap )
		{
            for (int i = 0; i < BitmapWidth; i++)
            {
                for (int j = 0; j < BitmapWidth; j++)
                {
                  
                    int pixVal = 256 * PixelsData[(BitmapWidth*2 * i) + (j*2)] +  PixelsData[(BitmapWidth*2 * i) + (j*2)+1];
                    //pBitmap->SetPixel(j, i, pixVal);
                    pBitmap->SetPixel(j, i, Gdiplus::Color(pixVal, pixVal, pixVal));
                }
            }
            pBitmap->GetHBITMAP(NULL, phbmp);
		}
	}
	GdiplusShutdown(token);
	if( *phbmp != NULL )
		return NOERROR;
	return E_NOTIMPL;

}


STDMETHODIMP CThumbnailProvider::GetSite(REFIID riid, 
                                         void** ppvSite)
{
    if (m_pSite)
    {
        return m_pSite->QueryInterface(riid, ppvSite);
    }
    return E_NOINTERFACE;
}


STDMETHODIMP CThumbnailProvider::SetSite(IUnknown* pUnkSite)
{
    if (m_pSite)
    {
        m_pSite->Release();
        m_pSite = NULL;
    }

    m_pSite = pUnkSite;
    if (m_pSite)
    {
        m_pSite->AddRef();
    }
    return S_OK;
}


STDAPI CThumbnailProvider_CreateInstance(REFIID riid, void** ppvObject)
{
    *ppvObject = NULL;

    CThumbnailProvider* ptp = new CThumbnailProvider();
    if (!ptp)
    {
        return E_OUTOFMEMORY;
    }

    HRESULT hr = ptp->QueryInterface(riid, ppvObject);
    ptp->Release();
    return hr;
}