#pragma once

#include <windows.h>
#include <shlobj.h>
#include <shlwapi.h>
#include <thumbcache.h>
#include <strsafe.h>

STDAPI_(ULONG) DllAddRef();
STDAPI_(ULONG) DllRelease();
STDAPI_(HINSTANCE) DllInstance();

// {DED320B4-03C9-4220-B646-4103A372093E}
#define szCLSID_StlThumbnailProvider L"{DED320B4-03C9-4220-B646-4103A372093E}"
DEFINE_GUID(CLSID_StlThumbnailProvider, 0xded320b4, 0x3c9, 0x4220, 0xb6, 0x46, 0x41, 0x3, 0xa3, 0x72, 0x9, 0x3e);


